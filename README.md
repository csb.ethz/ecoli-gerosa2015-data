# *E. coli* Gerosa 2015 data

Python module for loading metabolite concentrations and metabolic fluxes estimated in
the publication: 

Gerosa, Luca, et al. "Pseudo-transition analysis identifies the key regulators of
dynamic metabolic adaptations from steady-state data." *Cell systems* (2015)

## Usage

See `example.ipynb`.

## Disclaimer

The mapping between reactions and estimated fluxes has been created based on the
*e_coli_core* and *iJO1366* models. The mapping may be inaccurate for other/newer
models. If you find missing reactions in the mapping please submit them as merge
requests. 

## Author

Mattia Gollub, CSB group, ETH Zurich

This is only a collection of utility functions. The data belongs to the authors of the
publication cited above.