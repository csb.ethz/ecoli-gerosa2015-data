"""
"""

import os
from typing import List

import cobra
import numpy as np
import pandas as pd

CONDITIONS = [
    "Acetate",
    "Fructose",
    "Galactose",
    "Glucose",
    "Glycerol",
    "Gluconate",
    "Pyruvate",
    "Succinate",
]
CONDITIONS_SHORT = [
    "ac",
    "fru",
    "gal",
    "glc__D",
    "glyc",
    "glcn",
    "pyr",
    "succ",
]

_MICROMOLES_PER_MILLILITER_TO_MOLAR = 1e-6 / 1e-3
_ROOT = os.path.dirname(os.path.abspath(__file__))
_DATA_FILE = os.path.join(_ROOT, "data", "gerosa_2015_S2.xlsx")
_CONCENTRATIONS_SHEET_NAME = "Metabolite concentrations"
_FLUXES_SHEET_NAME = "Metabolic fluxes"
_CONVERSION_FACTORS_SHEET_NAME = "Conversion Factors"


def _normal_to_log_normal(series: pd.Series) -> pd.Series:
    # Find a log-normal approximation for normally distributed measurements.
    # In the absence of a better approximation, we use a sampling approach for the
    # conversion. Always use the same seed to ensure reproducibility.
    rng = np.random.default_rng(42)
    samples = np.maximum(rng.normal(series[0], series[1], 10000), 1e-10)
    log_samples = np.log(samples)
    series[0] = np.log(series[0])
    series[1] = np.std(log_samples)
    return series


def get_uM_per_gCDW_to_M_factor() -> float:
    """Get the conversion from the unit in which metabolite concentrations are reported
    the publication to molar.

    Returns
    -------
    float
        The conversion factor.
    """
    gCDW_to_milliliter = pd.read_excel(
        _DATA_FILE,
        sheet_name=_CONVERSION_FACTORS_SHEET_NAME,
        header=None,
        usecols="B",
        skiprows=5,
        nrows=1,
    ).iloc[0, 0]
    return _MICROMOLES_PER_MILLILITER_TO_MOLAR / gCDW_to_milliliter


def load_metabolite_concentrations(log_normal: bool = False) -> pd.DataFrame:
    """Load the measured concentrations, together with their uncertainties, in a
    dataframe and assign correct BiGG identifiers to the metabolites.

    Parameters
    ----------
    log_normal : bool, optional
        If true normal approximations for the log concentrations will be returned,
        otherwise the method will return the original data.

    Returns
    -------
    pd.DataFrame
        The measured metabolite concentrations.
    """

    # Read measurements in a dataframe.
    data_df = pd.read_excel(
        _DATA_FILE,
        sheet_name=_CONCENTRATIONS_SHEET_NAME,
        header=1,
        usecols="B,D:K,M:T",
        nrows=43,
    )
    data_df.columns = (
        ["met"] + CONDITIONS_SHORT + [c + "_std" for c in CONDITIONS_SHORT]
    )

    # Drop 3pg+2pg as we don't know how to handle sums of concentrations.
    data_df = data_df[data_df["met"] != "2pg+3pg"]

    # Convert metabolite IDs to proper BiGG IDs.
    data_df["met"] = (
        data_df["met"]
        .replace(
            {
                "glu": "glu__L",
                "gln": "gln__L",
                "asn": "asn__L",
                "asp": "asp__L",
                "tyr": "tyr__L",
                "arg": "arg__L",
                "phe": "phe__L",
                "cAMP": "camp",
            }
        )
        .map(lambda m: m.replace("-", "__") + "_c")
    )

    # Set metabolite names as index.
    data_df = data_df.set_index("met")

    # Convert to molar concentrations.
    data_df = data_df * get_uM_per_gCDW_to_M_factor()

    # Convert all values to log scale if needed.
    if log_normal:
        for i in range(8):
            data_df.iloc[:, [i, 8 + i]] = data_df.iloc[:, [i, 8 + i]].apply(
                lambda x: _normal_to_log_normal(x), axis=1
            )

    return data_df


def load_reaction_fluxes() -> pd.DataFrame:
    """Load the estimated fluxes, together with their uncertainties, in a dataframe.
    Values refers to the net fluxes defined in the publication.

    Returns
    -------
    pd.DataFrame
        The estimated fluxes.
    """

    # Read measurements in a dataframe.
    data_df = pd.read_excel(
        _DATA_FILE,
        sheet_name=_FLUXES_SHEET_NAME,
        header=1,
        index_col=0,
        usecols="A,B:I,K:R",
        nrows=36,
    )
    data_df.columns = (
        CONDITIONS_SHORT + [c + "_std" for c in CONDITIONS_SHORT]
    )

    return data_df


def get_reactions_to_fluxes_mapping(model: cobra.Model) -> np.ndarray:
    """Returns a matrix representing the linear transform from a vector of fluxes in the
    given E. coli model to the net fluxes estimated in the publication. Requires a model
    using BiGG reaction identifiers.

    Disclaimer: tested with the e_coli_core and iJO1366 models only. The mapping may be
    inaccurate for other/newer models. Please submit updates as merge requests to the
    repository.

    Parameters
    ----------
    model : cobra.Model
        The model for which the mapping must be built. This must use BiGG identifiers
        for the reactions.

    Returns
    -------
    pd.DataFrame
        A dataframe describing the linear mapping from the model to the estimated fluxes.
    """

    # Read the flux identifiers.
    flux_ids = pd.read_excel(
        _DATA_FILE,
        sheet_name=_FLUXES_SHEET_NAME,
        header=1,
        usecols="A",
        nrows=36,
    )["Flux"].tolist()
    rxn_ids = [r.id for r in model.reactions]

    # Build the mapping.
    rxn_to_flux = pd.DataFrame(0, index=flux_ids, columns=rxn_ids)
    def set_coefficient(ind, col, val):
        if col in rxn_to_flux.columns:
            rxn_to_flux.loc[ind, col] = val

    set_coefficient("Ace_Ex", "EX_ac_e", -1)
    set_coefficient("Fru_Ex", "EX_fru_e", -1)
    set_coefficient("Gal_Ex", "EX_gal_e", -1)
    set_coefficient("Glc_Ex", "EX_glc__D_e", -1)
    set_coefficient("Gly_Ex", "EX_glyc_e", -1)
    set_coefficient("Gln_Ex", "EX_glcn_e", -1)
    set_coefficient("Pyr_Ex", "EX_pyr_e", -1)
    set_coefficient("Suc_Ex", "EX_succ_e", -1)
    set_coefficient("Fum_Ex", "EX_fum_e", -1)
    set_coefficient("Lac_Ex", "EX_lac__D_e", -1)

    set_coefficient("PGI", "PGI", 1)
    set_coefficient("PGI", "PTS_to_PGI", 1)

    set_coefficient("PFK-FBP", "PFK", 1)
    set_coefficient("PFK-FBP", "FBP", -1)
    set_coefficient("PFK-FBP", "FBA_to_FBP", -1)
    set_coefficient("PFK-FBP", "TPI_to_FBP", -1)
    set_coefficient("PFK-FBP", "GAPD_to_FBP", -1)
    set_coefficient("PFK-FBP", "PFK_to_FBA", 1)
    set_coefficient("PFK-FBP", "PFK_to_TPI", 1)

    set_coefficient("FBA", "FBA", 1)
    set_coefficient("FBA", "FBA_to_FBP", -1)
    set_coefficient("FBA", "TPI_to_FBP", -1)
    set_coefficient("FBA", "GAPD_to_FBP", -1)
    set_coefficient("FBA", "PFK_to_FBA", 1)
    set_coefficient("FBA", "PFK_to_TPI", 1)

    set_coefficient("TPI", "TPI", 1)
    set_coefficient("TPI", "TPI_to_FBP", -1)
    set_coefficient("TPI", "GAPD_to_FBP", -1)
    set_coefficient("TPI", "PFK_to_TPI", 1)

    set_coefficient("Middle_EMP", "GAPD", 1)
    set_coefficient("Middle_EMP", "GAPD_to_FBP", -2)
    set_coefficient("Middle_EMP", "PPCK_to_GAPD", -1)
    set_coefficient("Middle_EMP", "PPS_to_GAPD", -1)

    set_coefficient("Low_EMP", "PGM", -1)
    set_coefficient("Low_EMP", "PGK_to_PGM", 1)
    set_coefficient("Low_EMP", "PPCK_to_PGM", -1)
    set_coefficient("Low_EMP", "PPCK_to_PGK", -1)
    set_coefficient("Low_EMP", "PPCK_to_GAPD", -1)
    set_coefficient("Low_EMP", "PPS_to_PGM", -1)
    set_coefficient("Low_EMP", "PPS_to_PGK", -1)
    set_coefficient("Low_EMP", "PPS_to_GAPD", -1)

    set_coefficient("PYK-PPS", "PYK", 1)
    set_coefficient("PYK-PPS", "PYK2", 1)
    set_coefficient("PYK-PPS", "PYK3", 1)
    set_coefficient("PYK-PPS", "PYK4", 1)
    set_coefficient("PYK-PPS", "PYK6", 1)
    set_coefficient("PYK-PPS", "PPS", -1)
    set_coefficient("PYK-PPS", "PPS_to_ENO", -1)
    set_coefficient("PYK-PPS", "PPS_to_PGM", -1)
    set_coefficient("PYK-PPS", "PPS_to_PGK", -1)
    set_coefficient("PYK-PPS", "PPS_to_GAPD", -1)
    set_coefficient("PYK-PPS", "OAADC", 1)

    set_coefficient("PDH", "PDH", 1)
    set_coefficient("PDH", "PFL", 1)
    set_coefficient("PDH", "POR5", 1)

    set_coefficient("ED", "EDD", 1)

    set_coefficient("PPP_ED_branch", "PGL", 1)

    set_coefficient("Early_PPP", "GND", 1)

    set_coefficient("RPI", "RPI", -1)
    set_coefficient("RPE", "RPE", 1)

    set_coefficient("Non-ox_PPP", "TKT1", 1)

    set_coefficient("TKT2", "TKT2", 1)
    set_coefficient("TKT2", "FBA3", 1)

    set_coefficient("PPC", "PPC", 1)
    set_coefficient("PPCK", "PPCK", 1)
    set_coefficient("PPCK", "OAADC", 1)
    set_coefficient("PPCK", "PPCK_to_ENO", 1)
    set_coefficient("PPCK", "PPCK_to_PGM", 1)
    set_coefficient("PPCK", "PPCK_to_PGK", 1)
    set_coefficient("PPCK", "PPCK_to_GAPD", 1)

    set_coefficient("Early_TCA", "CS", 1)
    set_coefficient("Early_TCA", "MDH_to_CS", 1)
    set_coefficient("Early_TCA", "MDH_to_ACONT", 1)
    set_coefficient("Early_TCA", "CS_to_ACONT", 1)
    set_coefficient("Early_TCA", "CITL", -1)

    set_coefficient("ICDH", "ICDHyr", 1)

    set_coefficient("Int_TCA", "AKGDH", 1)

    set_coefficient("SUCDH3", "SUCDi", 1)
    set_coefficient("SUCDH3", "FRD2", -1)
    set_coefficient("SUCDH3", "FRD3", -1)

    set_coefficient("FUM", "FUM", 1)

    set_coefficient("MDH+MQO", "MDH", 1)
    set_coefficient("MDH+MQO", "MDH2", 1)
    set_coefficient("MDH+MQO", "MDH3", 1)
    set_coefficient("MDH+MQO", "MOX", 1)
    set_coefficient("MDH+MQO", "MDH_to_CS", 1)

    set_coefficient("ME", "ME1", 1)
    set_coefficient("ME", "ME2", 1)

    set_coefficient("Glx_Cycle", "ICL", 1)

    set_coefficient("Growth_rate", "BIOMASS", 1)
    set_coefficient("Growth_rate", "BIOMASS_Ecoli_core_w_GAM", 1)
    set_coefficient("Growth_rate", "BIOMASS_Ec_iJO1366_WT_53p95M", 1)
    set_coefficient("Growth_rate", "BIOMASS_Ec_iJO1366_core_53p95M", 1)
    set_coefficient("Growth_rate", "BIOMASS_Ec_iML1515_WT_75p37M", 1)
    set_coefficient("Growth_rate", "BIOMASS_Ec_iML1515_core_75p37M", 1)

    return rxn_to_flux
