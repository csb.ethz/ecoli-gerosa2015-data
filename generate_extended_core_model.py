import pathlib
from typing import List

import cobra
import cobrapy_bigg_client.client as client
import pandas as pd

DATA_DIR = pathlib.Path(__file__).parent.resolve() / "data"
E_COLI_CORE_PATH = str(DATA_DIR / "e_coli_core.xml")
EXTENSIONS_PATH = DATA_DIR / "e_coli_core_extensions.csv"
DELETIONS_PATH = DATA_DIR / "e_coli_core_deletions.csv"
EXTENDED_E_COLI_CORE_PATH = str(DATA_DIR / "e_coli_core_extended.xml")
SUBSTRATE_EXCHANGES = [
    "EX_glc__D_e",
    "EX_gal_e",
    "EX_fru_e",
    "EX_ac_e",
    "EX_pyr_e",
    "EX_succ_e",
    "EX_glcn_e",
    "EX_glyc_e",
]


def assert_growth(model: cobra.Model, exchanges: List[str], epsilon: float = 1e-2):
    """Assert that the model grows on the given list of substrates."""

    old_glc_bound = model.reactions.EX_glc__D_e.lower_bound
    model.reactions.EX_glc__D_e.lower_bound = 0
    solution = model.optimize()
    assert solution.status == "infeasible"

    for s in exchanges:
        r = model.reactions.get_by_id(s)
        old_bound = r.lower_bound
        r.lower_bound = -100
        solution = model.optimize()
        assert solution.status == "optimal"
        assert solution.objective_value > epsilon
        r.lower_bound = old_bound

    model.reactions.EX_glc__D_e.lower_bound = old_glc_bound


if __name__ == "__main__":
    # Load the core model
    model = cobra.io.read_sbml_model(E_COLI_CORE_PATH)

    # Download additional reactions form BiGG and add them to the model.
    extensions_df = pd.read_csv(EXTENSIONS_PATH, header=0)
    reactions = []
    for id, lb, ub in extensions_df.itertuples(index=False):
        r = client.get_reaction(id)
        r.lower_bound = lb
        r.upper_bound = ub
        # Saving models in cobrapy fails with annotations provided by
        # cobrapy_bigg_client and in absence of reaction names.
        r.annotation = {}
        r.name = id
        reactions.append(r)
    model.add_reactions(reactions)

    # Remove inaccurate reactions.
    deletions_df = pd.read_csv(DELETIONS_PATH, header=0)
    model.remove_reactions(
        [model.reactions.get_by_id(r.id) for r in deletions_df.itertuples(index=False)]
    )

    # Verify correctness of the model and save it.
    assert_growth(model, SUBSTRATE_EXCHANGES)
    cobra.io.write_sbml_model(model, EXTENDED_E_COLI_CORE_PATH)
